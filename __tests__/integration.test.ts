import { Types } from "mongoose";
import supertest from "supertest";
import { connectMongo, disconnectMongo } from "../helpers/connectdb.hepler";
import { oid } from "../helpers/objectid.helper";
import Article from "../models/article.model";
import server from "../server";

const DB_URI = "mongodb://localhost:27017/super-articles-test";
let ID: null | Types.ObjectId = null;
let ID2: null | Types.ObjectId = null;

beforeAll(() => {
	return connectMongo(DB_URI);
});

describe("create tests", () => {
	it("should create two articles", async () => {
		const result = await supertest(server)
			.post("/articles")
			.send({ content: "Text" });

		const result2 = await supertest(server)
			.post("/articles")
			.send({ content: "Text" });

		ID = oid(result.body._id);
		ID2 = oid(result2.body._id);

		expect(result.body).toMatchObject({
			content: "Text",
			duplicate_article_ids: [],
		});

		expect(result2.body).toMatchObject({
			content: "Text",
			duplicate_article_ids: [result.body._id],
		});
	});
});

describe("get article tests", () => {
	it("should get article by id", async () => {
		if (!ID || !ID2) return;

		const result = await supertest(server).get(`/articles/${ID.toHexString()}`);

		expect(result.body).toEqual({
			__v: 0,
			_id: ID.toHexString(),
			content: "Text",
			duplicate_article_ids: [ID2.toHexString()],
		});
	});

	it("should get all articles", async () => {
		if (!ID || !ID2) return;

		const result = await supertest(server).get("/articles");

		expect(result.body).toEqual({
			articles: [
				{
					__v: 0,
					_id: ID.toHexString(),
					content: "Text",
					duplicate_article_ids: [ID2.toHexString()],
				},
			],
		});
	});
});

describe("duplicate ids route tests", () => {
	it("should get duplucate groups", async () => {
		if (!ID || !ID2) return;

		const result = await supertest(server).get("/duplicate_groups");

		expect(result.body).toEqual({
			duplicate_groups: [[ID.toHexString(), ID2.toHexString()]],
		});
	});
});

afterAll(async () => {
	if (ID && ID2) {
		await Article.deleteOne({ _id: ID });
		await Article.deleteOne({ _id: ID2 });
	}

	await disconnectMongo();
});
