import { present } from "../helpers/nlp.helper";
import { getArticleDuplicateIds } from "../helpers/getDuplicates.helper";
import { generateOID, Thrower } from "../helpers/test.helpers";
import { oid } from "../helpers/objectid.helper";
import { Types } from "mongoose";

describe("nlp tests", () => {
	it("should return infinitive", () => {
		expect(present("walked")).toBe("walk");
		expect(present("compromised")).toBe("compromise");
	});
});

describe("oid generation tests", () => {
	it("should generate instance of Types.ObjectId", () => {
		expect(oid("1".repeat(24))).toBeInstanceOf(Types.ObjectId);
	});

	it("should fail on invalid length", () => {
		expect(() => oid("1".repeat(9))).toThrow();
		expect(() => oid("1".repeat(99))).toThrow();
	});

	it("should fail on invalid pattern", () => {
		expect(() => oid("a/b(0-".repeat(4))).toThrow();
	});
});

describe("getDuplicates tests", () => {
	it("should get text duplicates", () => {
		const etalonText = "123";
		const id2 = generateOID();

		const articles = [
			{
				_id: generateOID(),
				content: "456",
				duplicate_article_ids: [],
			},
			{
				_id: id2,
				content: "123",
				duplicate_article_ids: [],
			},
		];

		const duplicates = getArticleDuplicateIds(etalonText, articles);
		expect(duplicates).toEqual([id2]);
	});

	it("should normalize verbs", () => {
		const etalonText = "go";
		const id1 = generateOID();
		const id2 = generateOID();

		const articles = [
			{
				_id: id1,
				content: "went",
				duplicate_article_ids: [],
			},
			{
				_id: id2,
				content: "goes",
				duplicate_article_ids: [],
			},
			{
				_id: generateOID(),
				content: "walked",
				duplicate_article_ids: [],
			},
		];

		const duplicates = getArticleDuplicateIds(etalonText, articles);
		expect(duplicates).toEqual([id1, id2]);
	});
});

describe("throw on null or undefined tests", () => {
	it("should throw on null or undefined", () => {
		const thrower = new Thrower();

		expect(() => thrower.returnArg(null)).toThrow();
		expect(() => thrower.returnArg(undefined)).toThrow();
	});

	it("should not throw on other value types", () => {
		const thrower = new Thrower();

		expect(() => thrower.returnArg(1)).not.toThrow();
		expect(() => thrower.returnArg("str")).not.toThrow();
		expect(() => thrower.returnArg({ test: "passed" })).not.toThrow();
		expect(() => thrower.returnArg(["test1", "test2"])).not.toThrow();
	});
});
