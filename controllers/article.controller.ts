import { Handler } from "express";
import { oid } from "../helpers/objectid.helper";
import ArticleRepository from "../repositories/article.repository";

class ArticleController {
	private repostory: ArticleRepository = new ArticleRepository();

	public get: Handler = async (req, res) => {
		try {
			const result = await this.repostory.get();
			res.json({ articles: result });
		} catch (err) {
			res.status(400).json({ message: err.message });
		}
	};

	public groups: Handler = async (req, res) => {
		try {
			const result = await this.repostory.getDuplicateGroups();
			res.json({ duplicate_groups: result });
		} catch (err) {
			res.status(400).json({ message: err.message });
		}
	};

	public getById: Handler = async (req, res) => {
		try {
			const result = await this.repostory.getById(oid(req.params.id));
			res.json(result);
		} catch (err) {
			res.status(404).json({ message: err.message });
		}
	};

	public create: Handler = async (req, res) => {
		try {
			const result = await this.repostory.create({
				duplicate_article_ids: [],
				...req.body,
			});

			res.status(201).json(result);
		} catch (err) {
			res.status(400).json({ message: err.message });
		}
	};

	public update: Handler = async (req, res) => {
		try {
			const result = await this.repostory.update(oid(req.params.id), req.body);
			res.json(result);
		} catch (err) {
			res.status(404).json({ message: err.message });
		}
	};

	public delete: Handler = async (req, res) => {
		try {
			const result = await this.repostory.delete(oid(req.params.id));
			res.json(result);
		} catch (err) {
			res.status(404).json({ message: err.message });
		}
	};
}

export default ArticleController;
