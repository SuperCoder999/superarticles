import express from "express";
import router from "./routers";

const server = express();

server
	.use(express.json())
	.use(express.urlencoded({ extended: true }))
	.use(router);

export default server;
