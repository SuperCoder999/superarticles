import * as config from "./config/app.config";
import server from "./server";
import { config as configEnvironment } from "dotenv";
import { connectMongo } from "./helpers/connectdb.hepler";

configEnvironment();
connectMongo();

server.listen(config.PORT, "", () => {
	console.log(`Server is running on port ${config.PORT}`);
	console.log("Ready to accept requests");
});

export default server;
