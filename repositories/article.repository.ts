import { Types } from "mongoose";
import { getArticleDuplicateIds } from "../helpers/getDuplicates.helper";
import { throwOnNullOrUndefined } from "../helpers/throw.helper";
import Article, {
	ArticleType,
	ArticleTypeCreate,
} from "../models/article.model";

class ArticleRepository {
	@throwOnNullOrUndefined("Can't get articles")
	public getAll() {
		return Article.find({}).lean().exec() as Promise<ArticleType[]>;
	}

	@throwOnNullOrUndefined("Can't get articles")
	public async get() {
		const repo = new ArticleRepository();
		const allArticles = await repo.getAll();
		let idBlackList: Types.ObjectId[] = [];
		const articles: ArticleType[] = [];

		allArticles.forEach((article) => {
			if (!idBlackList.find((id) => id.equals(article._id))) {
				idBlackList = idBlackList.concat(article.duplicate_article_ids);
				articles.push(article);
			}
		});

		return articles;
	}

	@throwOnNullOrUndefined("Can't get articles")
	public async getDuplicateGroups() {
		const repo = new ArticleRepository();
		const allArticles = await repo.get();

		return allArticles.map((article) => [
			article._id,
			...article.duplicate_article_ids,
		]);
	}

	@throwOnNullOrUndefined("Can't get article")
	public getById(_id: Types.ObjectId) {
		return Article.findOne({ _id }).lean().exec() as Promise<ArticleType>;
	}

	@throwOnNullOrUndefined("Can't create article")
	public async create(data: ArticleTypeCreate) {
		return Article.create(data);
	}

	@throwOnNullOrUndefined("Can't update article")
	public async update(_id: Types.ObjectId, data: Partial<ArticleTypeCreate>) {
		const repo = new ArticleRepository();
		const article = await repo.getById(_id);

		const allArticles = (await Article.find({ $nor: [{ _id }] })
			.lean()
			.exec()) as ArticleType[];

		if (data.content) {
			const duplicateIds = getArticleDuplicateIds(data.content, allArticles);
			data.duplicate_article_ids = duplicateIds;

			if (article.duplicate_article_ids.length > duplicateIds.length) {
				article.duplicate_article_ids.map(async (dupId) => {
					if (!duplicateIds.includes(dupId)) {
						const dupArticle = await repo.getById(dupId);
						const newDupIds = [...dupArticle.duplicate_article_ids];
						const index = newDupIds.findIndex(_id.equals);
						newDupIds.splice(index, 1);

						return await Article.updateOne(
							{ _id: dupId },
							{ duplicate_article_ids: newDupIds }
						);
					}
				});
			} else if (article.duplicate_article_ids.length < duplicateIds.length) {
				duplicateIds.map(async (dupId) => {
					if (!article.duplicate_article_ids.includes(dupId)) {
						const dupArticle = await repo.getById(dupId);
						const newDupIds = [...dupArticle.duplicate_article_ids, _id];

						return await Article.updateOne(
							{ _id: dupId },
							{ duplicate_article_ids: newDupIds }
						);
					}
				});
			}
		}

		return await Article.updateOne({ _id }, data).lean().exec();
	}

	@throwOnNullOrUndefined("Can't delete article")
	public async delete(_id: Types.ObjectId) {
		const repo = new ArticleRepository();
		const deletingArticle = await repo.getById(_id);

		deletingArticle.duplicate_article_ids.forEach(async (articleId) => {
			const article = await repo.getById(articleId);
			const newDupIds = [...article.duplicate_article_ids];
			const index = newDupIds.findIndex((id) => id.equals(articleId));
			newDupIds.splice(index, 1);
			repo.update(articleId, { duplicate_article_ids: newDupIds });
		});

		return Article.deleteOne({ _id }).lean().exec();
	}
}

export default ArticleRepository;
