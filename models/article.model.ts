import { model, Schema, Types } from "mongoose";
import { getArticleDuplicateIds } from "../helpers/getDuplicates.helper";

const schema = new Schema(
	{
		content: {
			type: String,
			required: true,
		},
		duplicate_article_ids: [
			{
				type: Types.ObjectId,
			},
		],
	},
	{
		collection: "articles",
	}
);

schema.pre("save", async function (next) {
	const docs = (await Article.find({}).lean().exec()) as ArticleType[];
	const duplicateIds = getArticleDuplicateIds(this.get("content"), docs);
	this.set("duplicate_article_ids", duplicateIds);

	await Promise.all(
		docs
			.filter((doc) => duplicateIds.includes(doc._id))
			.map((doc) =>
				Article.updateOne(
					{ _id: doc._id },
					{
						duplicate_article_ids: [...doc.duplicate_article_ids, this._id],
					}
				)
			)
	);

	next();
});

export interface ArticleTypeCreate {
	content: string;
	duplicate_article_ids: Types.ObjectId[];
}

export interface ArticleType extends ArticleTypeCreate {
	_id: Types.ObjectId;
}

const Article = model("Artcle", schema);
export default Article;
