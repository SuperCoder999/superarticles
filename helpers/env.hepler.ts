export const getNumberEnv = (
	value: string | undefined,
	defaultValue: number
) => {
	if (value !== undefined) {
		return Number(value);
	}

	return defaultValue;
};

export const getStringEnv = (
	value: string | undefined,
	defaultValue: string
) => {
	return value ?? defaultValue;
};
