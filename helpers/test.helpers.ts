import { oid } from "./objectid.helper";
import { throwOnNullOrUndefined } from "./throw.helper";

export function generateOID() {
	const SYMBOLS = [
		"1",
		"2",
		"3",
		"4",
		"5",
		"6",
		"7",
		"8",
		"9",
		"a",
		"b",
		"c",
		"d",
		"e",
		"f",
	];

	const OID_LENGTH = 24;
	let id = "";

	for (let i = 0; i < OID_LENGTH; i++) {
		const index = Math.floor(Math.random() * SYMBOLS.length);
		const letter = SYMBOLS[index];
		id += letter;
	}

	return oid(id);
}

export class Thrower {
	@throwOnNullOrUndefined("ERROR")
	public returnArg(value: any) {
		return value;
	}
}
