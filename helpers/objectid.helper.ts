import { Types } from "mongoose";

export const oid = (id: string): Types.ObjectId => {
	return new Types.ObjectId(id);
};
