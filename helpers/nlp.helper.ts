import nlp from "compromise";

export const present = (value: string) => {
	const doc = nlp(value);
	doc.verbs().toInfinitive();

	return doc.text();
};
