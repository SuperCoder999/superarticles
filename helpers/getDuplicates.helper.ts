import { Types } from "mongoose";
import { compareTwoStrings } from "string-similarity";
import * as config from "../config/app.config";
import { ArticleType } from "../models/article.model";
import { present } from "./nlp.helper";

export const getArticleDuplicateIds = (
	etalonContent: string,
	others: ArticleType[]
) => {
	const ids: Types.ObjectId[] = [];

	others.forEach((other) => {
		const similarityRate = compareTwoStrings(
			present(etalonContent),
			present(other.content)
		);

		const similarityPercent = similarityRate * 100;

		if (similarityPercent >= config.DUPLICATE_PERCENTANGE) {
			ids.push(other._id);
		}
	});

	return ids;
};
