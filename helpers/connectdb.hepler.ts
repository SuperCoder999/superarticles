import mongoose from "mongoose";
import * as config from "../config/app.config";

export const connectMongo = (uri?: string) => {
	return mongoose.connect(uri ?? config.MONGO_URI, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useCreateIndex: true,
	});
};

export const disconnectMongo = () => {
	return mongoose.connection.close();
};
