export const throwOnNullOrUndefined = (errorMessage: string) => {
	return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
		const originalFunction = descriptor.value;

		function maybeThrowError(result: any) {
			if (result === null || result === undefined) {
				throw {
					message: errorMessage,
				};
			}

			return result;
		}

		if (originalFunction.constructor.name === "AsyncFunction") {
			descriptor.value = async (...args: any[]) => {
				const result = await originalFunction(...args);
				return maybeThrowError(result);
			};
		} else {
			descriptor.value = (...args: any[]) => {
				const result = originalFunction(...args);
				return maybeThrowError(result);
			};
		}
	};
};
