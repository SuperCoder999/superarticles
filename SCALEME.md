# Scale instructions

<p>To scale this app, add corresponding layers to controllers, helpers, models, repositories and routers folders.</p>
<p>To add config, add some constants to config/app.config.ts file or create new .config.ts files if you want.</p>
<p>There is 'dotenv' dependency in this project, so add your test environment to .env file.</p>
<p>Find .env file template in .env.example file</p>
