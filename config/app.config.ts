import { getNumberEnv, getStringEnv } from "../helpers/env.hepler";

export const PORT = getNumberEnv(process.env.PORT, 5000);

export const DUPLICATE_PERCENTANGE = getNumberEnv(
	process.env.DUPLICATE_PERCENTANGE,
	95
);

export const MONGO_URI = getStringEnv(
	process.env.MONGO_URI,
	"mongodb://localhost:27017/super-articles"
);
