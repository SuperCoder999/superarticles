import { Router } from "express";
import ArticleController from "../controllers/article.controller";

const router = Router();
const controller = new ArticleController();

router
	.get("/articles", controller.get)
	.get("/duplicate_groups", controller.groups)
	.get("/articles/:id", controller.getById)
	.post("/articles", controller.create)
	.put("/articles/:id", controller.update)
	.delete("/articles/:id", controller.delete);

export default router;
