import { Router } from "express";
import article from "./article.router";

const router = Router();
router.use("", article);

export default router;
