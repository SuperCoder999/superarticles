# Super articles

Super articles is a prototype of server, that can create articles and search their duplicates.

# Immediately after downloading run:

`npm install` or `npm i`.

# To run app:

- In debug mode: `npm start`
- In production mode: `npm run start:prod`

**Default app port is 5000. You can change it by adding/changing PORT variable in your .env file**

# To run app in docker container:

Ensure, that docker and docker-compose are installed on your computer and run `docker-compose up -d`. Daemon mode (`-d`) is required, because there are two containers in docker-compose.yml (mongo and my server) and they must run in parallel mode.

# To test app:

Run `npm test`

# To build typescript:

Run `npm run build`.
<br />
**Don't forget to add `run`. `npm build` is an incorrect command.**

# App routes:

- GET /articles returns {"articles": \[data\]}
- GET /articles/:id returns {article data}
- GET /duplicate_groups returns {"duplicate_groups": \[groups data\]}
- POST /articles returns {article data}
- PUT /articles/:id returns {mongo update result}
- DELETE /articles/:id returns {mongo delete result}

# WARNING

- id field is named \_id and is an instance of Mongoose.Types.ObjectId
- Routes are incorrectly structured (with empty paths) to place /articles and /duplicate_groups in root url (Not /api/articles and /api/articles/duplicate_groups)

<hr />

<p>I used nodejs server (express), because nodejs has NLP library 'compromise', that is best for this project's tasks.</p>
MongoDB is used, because it is simple to use and connect to express. It also doesn't require migrations and seeders. This makes development a bit easier.
<p>I skipped services layer, because there are no third-party APIs, etc.</p>
<p>For searching duplicates used 'string-similarity' library. For normalizing verbs form - compromise.</p>
<p>For testing I used jest, because it is very easy and jest's plugin TS-jest to test typescript files. For testing the whole app I used supertest library. It is able to start express app and send a single request to it.</p>
